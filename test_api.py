 #!/usr/bin/env python3
from _game_database import _game_database
import unittest

class finalProjectTest(unittest.TestCase):
        """ unit tests for final project """ 

        port_number = 52093
        game_id = 10
        game_title = 'Counter-Strike'
        gdb = _game_database()

        user_id = 1

        image_id = 620
        image_url = '620'

        def reset_game(self):
                self.gdb.delete_all_games()
                self.gdb.load_games('data/games.dat')                


        def test_get_game(self):
                ''' test for getting the game '''
                self.reset_game()
                game = self.gdb.get_game(self.game_id)
                self.assertEqual(game[0], self.game_title)
        
        def test_set_game(self):
                ''' test for setting a new game '''
                self.reset_game()
                game = self.gdb.get_game(self.game_id)
                game[0] = 'Test_title'
                self.gdb.set_game(1, game)
                game = self.gdb.get_game(1)
                self.assertEquals(game[0], 'Test_title')

        def test_delete_game(self):
                self.reset_game()
                self.gdb.delete_game(3)
                game = self.gdb.get_game(3)
                self.assertEquals(game, None)


        # Users
        def reset_user(self):
                self.gdb.delete_all_users()
                self.gdb.load_users('data/users.dat')


        def test_get_user(self):
                ''' test for getting the user '''
                self.reset_user()
                user = self.gdb.get_user(self.user_id)
                self.assertEqual(user[0], 'F')
        
        def test_set_user(self):
                ''' test for setting a new user '''
                self.reset_user()
                user = self.gdb.get_user(self.user_id)
                user[0] = 'Test_title'
                self.gdb.set_user(1, user)
                user = self.gdb.get_user(1)
                self.assertEquals(user[0], 'Test_title')

        def test_delete_user(self):
                self.reset_user()
                self.gdb.delete_user(3)
                user = self.gdb.get_user(3)
                self.assertEquals(user, None)



        # Images
        def reset_images(self):
                #self.gdb.delete_all_images()
                self.gdb.load_images('data/images.dat')


        def test_get_images(self):
                self.reset_images()
                image = self.gdb.get_image(self.image_id)
                self.assertEqual(image[0], self.image_url)

if __name__ == '__main__':
        unittest.main()
