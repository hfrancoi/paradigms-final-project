#!/usr/bin/env python3

class _game_database:

	def __init__(self):
		self.games = {}
		self.users = {}

	def load_games(self, game_file):
		self.games = {}
		f = open(game_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			gid = int(components[0])
			gtitle = components[1]
			gpos = components[2]
			gneg = components[3]
			gpercent = components[4]
	
			self.games[gid] = components
		f.close()
		
	def get_game(self, gid):
		if gid in self.games:
			return [self.games[gid][1], self.games[gid][2], self.games[gid][3], self.games[gid][4]]
		return None

	def get_games(self):
		id_list = []
		for id in self.games:
			id_list.append(int(id))
		return id_list

	def set_game(self, gid, other_list):
		self.games[gid] = [str(gid), other_list[0], other_list[1], other_list[2], other_list[3]]

	def delete_game(self, gid):
		if gid in self.games:
			del self.games[gid]
	
	def delete_all_games(self):
		self.games = {}

	def load_users(self, users_file):
		self.users = {}
		f = open(users_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			uid = int(components[0])
			
			self.users[uid] = components
		f.close()

	def get_user(self, uid):
		if uid in self.users:
			return [self.users[uid][1], int(self.users[uid][2]), self.users[uid][3]]
		return None
				
	def get_users(self):
		id_list = []
		for id in self.users:
			id_list.append(int(id))
		return id_list
		
	def set_user(self, uid, other_list):
		self.users[uid] = [str(uid), other_list[0], other_list[1], other_list[2]]
	
	def delete_user(self, uid):
		if uid in self.users:
			del self.users[uid]
					
	def delete_all_users(self):
		self.users = {}

	def load_images(self, images_file):
		self.images = {}
		f = open(images_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			iid = int(components[0])
			
			self.images[iid] = components
		f.close()
	
	def get_image(self, iid):
		if iid in self.images:
			return self.images[iid]
		return None
	
if __name__ == "__main__":
	gdb = _game_database()
	
	#### GAMES ####
	gdb.load_games('data/games.dat')

	#### USERS ####
	gdb.load_users('data/users.dat')

	#### IMAGES ####
	gdb.load_images('data/images.dat')
