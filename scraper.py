#!/usr/bin/env python3

import sys
import urllib.request

for line in sys.stdin:
	input = line.rstrip().split("::")[0]

	# Make request to steam store link for game id given
	response = urllib.request.urlopen("https://store.steampowered.com/app/" + input)
	
	# Iterate through html until we find images
	urls = ['dummy']
	for line in response.readlines():
		line = line.decode("utf8")
		if "<img" in line:
			correct_string = '/apps/' + str(input)
			# Get game cover image
			if 'game_header_image_full' in line:
				line = line.rstrip().split("src=")[1]
				line = line.split('"')[1]
				urls[0] = line
			# Get slideshow/other images
			elif correct_string in line:
				line = line.rstrip().split("src=")[1]
				line = line.split('"')[1]
				urls.append(line)
	for url in urls:
		print(url)
	
