import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/Observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

const url = 'http://student04.cse.nd.edu:52093/';

/*
Generated class for the GamesProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class GamesProvider {
  
  gid: number;
  uid: number = 13;
  current: number = 0;
  games_observable: Observable<any> = null;
  games: any = null;
  
  user_observable: Observable<any> = null;
  user: any = null;
  
  constructor(public http: HttpClient) {
    this.gid = 10;
    this.getGames();
    this.getUser();
  }
  
  getUser(){
    if(this.user) return Observable.of(this.user);
    if(this.user_observable) return this.user_observable;
    this.user_observable = this.http.get(url + 'users/' + this.uid);
    this.user_observable.subscribe(user => {
      this.user = user;
    });
    return this.user_observable;
  }
  
  getGame(value: number): Observable<any> {
    this.gid = value;
    return this.http.get(url + 'games/' + value);
  }
  
  getNextGame(): Observable<any> {
    const next = this.current;
    this.current ++;
    if(this.games){
      return this.getGame(this.games[next].id);
    } else {
      return this.games_observable.switchMap(games => this.getGame(this.games[next].id));
    }
  }
  
  getGames(){
    if(this.games_observable) return;
    this.games_observable = this.http.get(url + 'games/');
    this.games_observable.subscribe(games => {
      console.log(games);
      this.games = games.games;
    });
  }
  
  getImages(gid: number){
    return this.http.get(url + 'images/' + gid);
  }
  
  sendVote(value: number){
    const f = user => {
      console.log('new push', user);
      let payload = {
        ...user,
        games_owned: user.games_owned + '|' + this.gid
      };
      this.http.put(url + 'users/' + this.uid, JSON.stringify(payload)).subscribe();
      this.user = payload;
    }
    if(this.user){
      f(this.user);
    } else {
      return this.user_observable.subscribe(userObj => f(userObj.user));
    }
  }
  

  resetUser(){
    this.current = 0;
    this.user = null;
    this.user_observable = null;
    this.getUser();
    this.http.delete(url + 'users/' + this.uid).subscribe();
  }
}
