import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GamesProvider } from '../../providers/games/games';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  game$: Observable<any>;
  images$: Observable<any>;
  user$: Observable<any>;

  constructor(public navCtrl: NavController, private gameCtrl: GamesProvider) {
    this.game$ = this.gameCtrl.getNextGame();
    this.game$.subscribe(game => this.images$ = this.gameCtrl.getImages(game.id));
    this.user$ = this.gameCtrl.getUser();
  }

  vote(value: number){
    this.gameCtrl.sendVote(value);
    this.game$ = this.gameCtrl.getNextGame();
    this.game$.subscribe(game => this.images$ = this.gameCtrl.getImages(game.id));
    this.user$ = this.gameCtrl.getUser();
  }

  resetUser(){
    this.gameCtrl.resetUser();
    this.game$ = this.gameCtrl.getNextGame();
    this.game$.subscribe(game => this.images$ = this.gameCtrl.getImages(game.id));
    this.user$ = this.gameCtrl.getUser();
  }
}
