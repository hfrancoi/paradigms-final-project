#!/usr/bin/env python3

import json
import requests
import cherrypy
from _game_database import _game_database

class UserController(object):
	def __init__(self, gdb):
		self.my_gdb = gdb

	# Event handlers for resource requests
	
	def GET(self):
		mylist = []
		for key in self.my_gdb.get_users():
			value = self.my_gdb.get_user(int(key))
			elem = {'id' : int(key), 'gender' : value[0], 'age' : int(value[1]), 'games_owned' : str(value[2])}
			mylist.append(elem)
		output = {'users' : mylist, 'result' : 'success'}
		return json.dumps(output)
		
	def POST(self):
		output = {'result' : 'success'}
		data = json.loads(cherrypy.request.body.read())
		try:
			gender = data['gender']
			age = data['age']
			games_owned = data['games_owned']
			key = self.my_gdb.get_users()[-1]+1
			output['id'] = key
			self.my_gdb.set_user(key, [gender, age, games_owned])
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
		
	def DELETE(self):
		self.my_gdb.delete_all_users()
		return json.dumps({'result' : 'success'})
		
	def GET_KEY(self, key):
		output = {'result' : 'success'}
		key = str(key)
		try:
			value = self.my_gdb.get_user(int(key))
			if value:
				output['id'] = key
				output['gender'] = value[0]
				output['age'] = int(value[1])
				output['games_owned'] = str(value[2])
			else:
				output['result'] = 'error'
				output['message'] = 'None type value associated with requested key'
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
		
	def PUT_KEY(self, key):
		output = {'result' : 'success'}
		key = int(key)
		# extract message from body
		data = json.loads(cherrypy.request.body.read())
		try:
			gender = data['gender']
			age = data['age']
			games = data['games_owned']
			self.my_gdb.set_user(key, [gender, age, games])
		except:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
		
	def DELETE_KEY(self, key):
		output = {'result' : 'success'}
		key = int(key)
		self.my_gdb.delete_user(key)
		return json.dumps(output)
