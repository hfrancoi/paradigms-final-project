#!/usr/bin/env python3
import json
import requests
import cherrypy
from _game_database import _game_database

class GameController(object):
	def __init__(self, gdb):
		self.my_gdb = gdb
	
	def get_value(self, key):
		key = int(key)
		return self.my_gdb.get_game(key)
		
	# Event handlers for resource requests
	
	def GET(self):
		mylist = []
		for key in self.my_gdb.get_games():
			value = self.my_gdb.get_game(int(key))
			elem = {'id' : int(key), 'title' : value[0], 'positive' : value[1], 'negative' : value[2], 'percent' : value[3]}
			mylist.append(elem)
		output = {'games' : mylist, 'result' : 'success'}
		return json.dumps(output)
		
	def POST(self):
		output = {'result' : 'success'}
		data = json.loads(cherrypy.request.body.read())
		try:
			title = data['title']
			positive = data['positive']
			negative = data['negative']
			percent = data['percent']
			key = self.my_gdb.get_games()[-1]+1
			output['id'] = key
			self.my_gdb.set_game(key, [title, positive, negative, percent])
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
	
	def DELETE(self):
		self.my_gdb.delete_all_games()
		return json.dumps({'result' : 'success'})
	
	def GET_KEY(self, key):
		output = {'result' : 'success'}
		key = str(key)
		try:
			value = self.get_value(key)
			if value:
				output['id'] = key
				output['title'] = value[0]
				output['positive'] = value[1]
				output['negative'] = value[2]
				output['percent'] = value[3]
				
				#img = 'not found'
				#f = open('/home/paradigms/images.dat')
				#for line in f:
				#	line = line.rstrip()
				#	components = line.split("::")
				#	if components[0] == str(key) and len(components) > 2:
				#		img = components[2]
				#f.close()
				#output['img'] = img
			else:
				output['result'] = 'error'
				output['message'] = 'None type value associated with requested key'
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)

	def PUT_KEY(self, key):
		output = {'result' : 'success'}
		key = int(key)
		# extract message from body
		data = json.loads(cherrypy.request.body.read())
		try:
			title = data['title']
			positive = data['positive']
			negative = data['negative']
			percent = data['percent']
			self.my_gdb.set_game(key, [title, positive, negative, percent])
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
		
	def DELETE_KEY(self, key):
		output = {'result' : 'success'}
		key = int(key)
		self.my_gdb.delete_movie(key)
		return json.dumps(output)
