#!/usr/bin/env python3

import sys
from _game_database import _game_database

if __name__ == '__main__':
	gdb = _game_database()
	gdb.load_games('games.dat')
	gdb.load_users('users.dat')
	
	# Tests:
	
	# get_game
	print(gdb.get_game(10))
	
	# set_game
	gdb.set_game(10, ['Test Name', 60, 40, '60%'])
	print(gdb.get_game(10))
	
	# delete_game
	gdb.delete_game(10)
	print(gdb.get_game(10))
	
	# get_user
	print(gdb.get_user(200))
	
	# set_user
	gdb.set_user(200, ['M', 29, '20|400'])
	print(gdb.get_user(200))
	
	# delete_user
	gdb.delete_user(200)
	print(gdb.get_user(200))
