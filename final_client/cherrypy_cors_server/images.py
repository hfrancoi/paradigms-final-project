#!/usr/bin/env python3

import json
import requests
import cherrypy
from _game_database import _game_database

class ImageController(object):
	def __init__(self, gdb):
		self.my_gdb = gdb

	# Event handler for resource requests
	def GET_KEY(self, key):
		output = {'result' : 'success'}
		key = str(key)
		try:
			value = self.my_gdb.get_image(int(key))
			if value:
				output['id'] = key
				output['images'] = value[1:]
			else:
				output['result'] = 'error'
				output['message'] = 'None type value associated with requested key'
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)
