# main.py

import cherrypy
import json
from _game_database import _game_database
from games import GameController
from images import ImageController
from users import UserController
from reset import ResetController
from options import OptionsController

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"

def start_service(gdb):
	# Controllers
	game_con 	= GameController(gdb)
	image_con	= ImageController(gdb)
	user_con 	= UserController(gdb)
	reset_con	= ResetController(gdb)
	options_con = OptionsController(gdb)
	
	# Create a dispatcher
	dispatcher = cherrypy.dispatch.RoutesDispatcher()
	
	# Movie connections
	dispatcher.connect('games_get', '/games/', controller=game_con, action='GET', conditions=dict(method=['GET']))
	dispatcher.connect('games_post', '/games/', controller=game_con, action='POST', conditions=dict(method=['POST']))
	dispatcher.connect('games_delete', '/games/', controller=game_con, action='DELETE', conditions=dict(method=['DELETE']))
	dispatcher.connect('games_get_key', '/games/:key', controller=game_con, action='GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('games_put_key', '/games/:key', controller=game_con, action='PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('games_delete_key', '/games/:key', controller=game_con, action='DELETE_KEY', conditions=dict(method=['DELETE']))
	# User connections
	dispatcher.connect('users_get', '/users/', controller=user_con, action='GET', conditions=dict(method=['GET']))
	dispatcher.connect('users_post', '/users/', controller=user_con, action='POST', conditions=dict(method=['POST']))
	dispatcher.connect('users_delete', '/users/', controller=user_con, action='DELETE', conditions=dict(method=['DELETE']))
	dispatcher.connect('users_get_key', '/users/:key', controller=user_con, action='GET_KEY', conditions=dict(method=['GET']))
	dispatcher.connect('users_put_key', '/users/:key', controller=user_con, action='PUT_KEY', conditions=dict(method=['PUT']))
	dispatcher.connect('users_delete_key', '/users/:key', controller=user_con, action='DELETE_KEY', conditions=dict(method=['DELETE']))
	# Image connections
	dispatcher.connect('images_get_key', '/images/:key', controller=image_con, action='GET_KEY', conditions=dict(method=['GET']))
	# Reset connections
	dispatcher.connect('reset_put', '/reset/', controller=reset_con, action='PUT', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_put_key', '/reset/:key', controller=reset_con, action='PUT_KEY', conditions=dict(method=['PUT']))
	# Option connections
	dispatcher.connect('options_1', '/games/', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_2', '/games/:key', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_3', '/users/', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_4', '/users/:key', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_5', '/images/:key', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_6', '/reset/', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('options_7', '/reset/:key', controller=options_con, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	
	# configuration for server
	conf = {
			'global' : {
					'server.socket_host' : 'student04.cse.nd.edu',
					'server.socket_port' : 52093,
				},
			'/' : { 'request.dispatch' : dispatcher,
					'tools.CORS.on' : True },
			}
	# update configuration
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

# Main
if __name__ == '__main__':
	gdb = _game_database()
	gdb.load_games('data/games.dat')
	gdb.load_users('data/users.dat')
	gdb.load_images('data/images.dat')
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service(gdb)
