# Paradigms-Final-Project

Our final project for Programming Paradigms


Notes on games.dat:
Information is provided in a :: separated format like so:

game_id::title::positive_reviews::negative_reviews::percentage_rating


Notes on users.dat:
Information is provided in a :: separated format like so:

user_id::gender::age::games_owned

(note that games_owned is a | separated list of individual game ids,
ex: 1000|2393|8473|922|2|2889)


How to run tests:
Simply run our test file test_api.py to test our methods, you can change specifics
if needed to test for different games/users.
