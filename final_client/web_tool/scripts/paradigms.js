// paradigms.js
// Kieran McDonald
// Javascript Milestone


function Item() {
	this.addToDocument = function(){
		document.body.appendChild(this.item)
	}
}

function Label() {
	this.createLabel = function(text, id){
		this.item = document.createElement("p")
		this.item.setAttribute("id", id)
		this.textLabel = document.createTextNode(text)
		this.item.appendChild(this.textLabel)
	},
	this.setText = function(text){
		this.item.innerHTML = text
	}
}

function Button() {
	this.createButton = function(text, id){
		this.item = document.createElement("BUTTON")
		this.item.setAttribute("id", id)
		this.button = document.createTextNode(text)
		this.item.appendChild(this.button)
	},
	this.addClickEventHandler = function(handler, args){
		this.item.onmouseup = function(args){
			handler(args)
		}
	}
}

function Div() {
	this.createDiv = function(id){
		this.item = document.createElement("div")
		this.item.setAttribute("id", id)
	}
	// this.addNewElementToDiv = function
}

function Image() {
	this.createImg = function(id) {
		this.item = document.createElement("img")
		this.item.setAttribute("id", id)
	}
	//this.img.src = http://student04.cse.nd.edu/skumar5/images/
	this.setImg = function(link){
		this.item.src = "http://student04.cse.nd.edu/skumar5/images/" + link
	}
}

function Dropdown() {
	this.createDropdown = function(dict, id, selected) {
		this.item = document.createElement("select")
		this.item.setAttribute("id", id)
		this.item.setAttribute("name", selected)
		//for (var i = 0; i < 5; i++){
		for (var elem in dict){
			//console.log(i)
			this.option = document.createElement("option")
			this.option.text = elem
			this.option.value = dict[elem]
			this.item.add(this.option)
		}
	},
	this.getSelected = function() {
		return this.item.options[this.item.selectedIndex].value
	}
}
