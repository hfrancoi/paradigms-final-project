// main.js

// Prototypes
Label.prototype 	= new Item()
Button.prototype 	= new Item()
Image.prototpye 	= new Item()


window.onload = function(){
	document.getElementById("up_button").onmouseup = submitVote
	document.getElementById("down_button").onmouseup = getNextGame
	current_game = 620
	getGames(current)
}

var current = 0

function getNextGame(){
	current = current + 1
	getGames(current)
}

function getGames(current){
	var gid = 10
	var xhrg = new XMLHttpRequest()
	xhrg.open("GET", "http://student04.cse.nd.edu:52093/games/", true)
	xhrg.onload = function(e){
		var text = JSON.parse(xhrg.responseText)
		console.log(text)
		gid = text.games[current].id
		console.log(gid)
		changeText(gid)
	}
	xhrg.onerror = function(e){
		console.error(xhrg.statusText)
	}
	xhrg.send(null)
}

function changeText(gid){
	// response stuff
	var xhr0 = new XMLHttpRequest()
	var xhr1 = new XMLHttpRequest()
	var xhr2 = new XMLHttpRequest()
	xhr0.open("GET", "http://student04.cse.nd.edu:52093/games/" + gid, true)
	xhr0.onload = function(e){
		window.current_game = gid
		var text = JSON.parse(xhr0.responseText)
		//console.log(text)
		document.getElementById("title_label").innerHTML = text.title
		document.getElementById("percent_label").innerHTML = text.percent
		document.getElementById("user_label").innerHTML = "User ID: " + 12
		xhr1.open("GET", "http://student04.cse.nd.edu:52093/images/" + gid, true)
		xhr2.open("GET", "http://student04.cse.nd.edu:52093/users/" + 12, true)
		xhr1.send(null)
		xhr2.send(null)
	}
	xhr1.onload = function(e){
		console.log(xhr1.responseText)
		var images = JSON.parse(xhr1.responseText).images
		document.getElementById("image_id").src = images[0]
		document.getElementById("img1").src = images[2]
		document.getElementById("img2").src = images[3]
		document.getElementById("img3").src = images[4]
		document.getElementById("img4").src = images[5]
	}
	xhr2.onload = function(e){
		var games = JSON.parse(xhr2.responseText).games_owned
		console.log(games)
		document.getElementById("games_label").innerHTML = "Games: " + games
	}
	xhr0.onerror = function(e){
		console.error(xhr0.statusText)
	}
	xhr1.onerror = function(e){
		console.error(xhr1.statusText)
	}
	xhr2.onerror = function(e){
		console.error(xhr2.statusText)
	}
	xhr0.send(null)
}

function submitVote(){
	var xhr4 = new XMLHttpRequest()
	var xhr5 = new XMLHttpRequest()
	xhr4.open("GET", "http://student04.cse.nd.edu:52093/users/12", true)
	xhr4.onload = function(e){
		var thing = JSON.parse(xhr4.responseText)
		var gender = thing.gender
		var age = thing.age
		var games_owned = thing.games_owned
		console.log("hey" + games_owned)
		games_owned = games_owned + "|" + window.current_game
		xhr5.open("PUT", "http://student04.cse.nd.edu:52093/users/12", true)
		xhr5.onload = function(e){
			console.log("here")
			getNextGame()
		}
		xhr5.onerror = function(e){
			console.error(xhr5.statusText)
		}
		xhr5.send(JSON.stringify({"gender" : gender, "age" : age, "games_owned" : games_owned}))
	}
	xhr4.onerror = function(e){
		console.error(xhr4.statusText)
	}
	xhr4.send(null)
}
