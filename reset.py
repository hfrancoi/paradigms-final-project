#!/usr/bin/env python3

import json
import requests
import cherrypy
from _game_database import _game_database

class ResetController(object):
	def __init__(self, gdb):
		self.gdb = gdb
	
	def PUT(self):
		self.gdb.load_games('data/games.dat')
		self.gdb.load_users('data/users.dat')
		
		return json.dumps({'result' : 'success'})
	
	def PUT_KEY(self, key):
		dict_of_games = {}
		f = open('data/games.dat')
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			gid = int(components[0])
			
			dict_of_movies[gid] = components
		f.close()
		
		tg_list = dict_of_games[int(key)][1:]
		self.gdb.set_game(int(key), tg_list)
		return json.dumps({'result' : 'success'})
